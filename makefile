#
#make file for treevolve
#

treevolve: const.c const.h const.sub.c const.sub.h coseq.c coseq.h exp.c exp.h exp.sub.c exp.sub.h mathfuncs.c mathfuncs.h models.c models.h nodestack.c nodestack.h treevolve.c treevolve.h 
	cc -o treevolve const.c const.sub.c coseq.c exp.c exp.sub.c mathfuncs.c models.c nodestack.c treevolve.c -lm

clean:	rm *.o
