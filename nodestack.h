/* header file for nodestack.c */
/* (c) N Grassly 1997          */
/* Dept. Zoology, Oxford.      */


/*--- prototypes ---*/

Node *FirstNodePop();
Node *NodePop(Node *prev);
void Stack(Node *going);

