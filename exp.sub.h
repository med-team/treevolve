/* header file for exp.sub.c   */
/* (c) N Grassly 1997          */
/* Dept. Zoology, Oxford.      */

#ifndef _EXPSUB_H_
#define _EXPSUB_H_


#include "treevolve.h"

int EpiSubRoutine(Regime *pp);
double epiSubfunc(double t);

#endif /* _EXPSUB_H_ */

