/* coseq.c - source file for TREEVOLVE       */
/* sequence evolution bit                    */
/* (c) Nick Grassly and Andrew Rambaut 1997  */
/*     Dept. of Zoology, Univ. of Oxford,    */
/*     nicholas.grassly@zoo.ox.ac.uk         */
/*     http://evolve.zoo.ox.ac.uk/           */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include "treevolve.h"
#include "coseq.h"
#include "models.h"
#include "mathfuncs.h"
#ifdef __MWERKS__
#include <console.h>
#endif

/* varables */
int tipNo;

double gammaShape;
int numCats, rateHetero;
double catRate[MAX_RATE_CATS];
double freqRate[MAX_RATE_CATS];

char *nucleotides="ACGT";
static double matrix[MAX_RATE_CATS][16];
static double vector[4];
static double *siteRates;
static short *categories;

/* protoypes */
void SeqEvolve();
void Mutate(Node *node);
char SetBase(double *P);
void RandomSequence(char *seq);
void MutateSequence(char *seq, double len);
void WriteSequence(int sampleNo, char *P);

void SeqEvolve ()
{
	tipNo=0;
	RandomSequence(first->sequence);
	SetCategories();
	fprintf(stdout, " %d %d\n", sampleSize, numBases);
	Mutate(first);
	Stack(first);
}

void Mutate(Node *node)
{
int i, expNo, posn;
float xm;
char *point, *p1, *p2, *child;
double rnd;

	if(node->type==1){						/*i.e. recombination event */
		if(node->daughters[1]->type==1)		/* first encounter */
			node->type=3;
		else{								/* second encounter */
			p1=node->sequence;
			p2=node->daughters[1]->sequence;/* remember this is spare for re and points to other parent */
			child=node->daughters[0]->sequence;
			for(i=0;i<numBases;i++){
				if(node->ancestral[i]==1)
					*child=*p1;
				else
					*child=*p2;
				child++;
				p1++;
				p2++;
				
			}
			*child='\0';
			
			MutateSequence(node->daughters[0]->sequence, (mutRate*((double) node->time - (double) node->daughters[0]->time)));
			
			if(node->daughters[0]->type==2){
				tipNo++;
				WriteSequence(tipNo, node->daughters[0]->sequence);
			}else
				Mutate(node->daughters[0]);
		
			Stack(node->daughters[1]);		/* stack up memory */
			if(node->daughters[0]->type!=3)
				Stack(node->daughters[0]);
		}
	}else{									/* a coalescent event */
		memcpy(node->daughters[0]->sequence, node->sequence, (numBases+1));
		MutateSequence(node->daughters[0]->sequence, (mutRate*((double) node->time - (double) node->daughters[0]->time)));
		if(node->daughters[0]->type==2){
			tipNo++;
			WriteSequence(tipNo, node->daughters[0]->sequence);
		}else
			Mutate(node->daughters[0]);
		if(node->daughters[0]->type!=3)
			Stack(node->daughters[0]);		/* stack up memory */
		
		memcpy(node->daughters[1]->sequence, node->sequence, (numBases+1));
		MutateSequence(node->daughters[1]->sequence, (mutRate*((double) node->time - (double) node->daughters[1]->time)));
		if(node->daughters[1]->type==2){
			tipNo++;
			WriteSequence(tipNo, node->daughters[1]->sequence);
		}else
			Mutate(node->daughters[1]);
			
		if(node->daughters[1]->type!=3)
			Stack(node->daughters[1]);		/* stack up memory */
	}
	
}

void RateHetMem()
{
	if (rateHetero==GammaRates){
		if( (siteRates=(double*)calloc(numBases, sizeof(double)))==NULL){
			fprintf(stderr, "Out of memory allocating site specific rates (RateHetMem)\n");
			exit(0);
		}
	}else if (rateHetero==DiscreteGammaRates){
		if( (categories=(short*)calloc(numBases, sizeof(short)))==NULL){
			fprintf(stderr, "Out of memory allocating discrete gamma categories (RateHetMem)\n");
			exit(0);
		}
	}
}


void SetCategories()
{
	int cat, i, j;
	double sumRates;
	
	if (rateHetero==CodonRates) {
		sumRates=catRate[0]+catRate[1]+catRate[2];
		if (sumRates!=3.0) {
			catRate[0]*=3.0/sumRates;
			catRate[1]*=3.0/sumRates;
			catRate[2]*=3.0/sumRates;
		}
	} else if (rateHetero==GammaRates) {
		for (i=0; i<numBases; i++)
			siteRates[i]=rndgamma(gammaShape) / gammaShape;

	} else if (rateHetero==DiscreteGammaRates) {
		DiscreteGamma(freqRate, catRate, gammaShape, gammaShape, numCats, 0);
		for (i=0; i<numBases; i++)
			categories[i]=(int)(rndu()*numCats);
	}
}


char SetBase(double *P)
{
	char j;
	double r;
	
	r=rndu();
	for (j='\0'; r>(*P) && j<'\3'; j++) P++;
	return j;
}


void RandomSequence(char *seq)
{
	int i;
	char *P;
	
	P=seq;
	for (i=0; i<numBases; i++) {
		*P=SetBase(addFreq);
		P++;
	}
	*P='\0';
}

void MutateSequence(char *seq, double len)
{
	int i, cat;
	double *Q;
	short *R;
	char *P;
	
	P=seq;
	
	switch (rateHetero) {
		case GammaRates:
			Q=siteRates;
			
			for (i=0; i<numBases; i++) {
				SetVector(vector, *P, (*Q)*len);
				*P=SetBase(vector);
				P++;
				Q++;
			}
		break;
		case DiscreteGammaRates:
			for (i=0; i<numCats; i++)
				SetMatrix(matrix[i], catRate[i]*len);
			
			R=categories;
			for (i=0; i<numBases; i++) {
				*P=SetBase(matrix[*R]+(*P<<2));
				P++;
				R++;
			}
		break;
		case CodonRates:
			for (i=0; i<numCats; i++)
				SetMatrix(matrix[i], catRate[i]*len);
			
			for (i=0; i<numBases; i++) {
				cat=i%3;
				*P=SetBase(matrix[cat]+(*P<<2));
				P++;
			}
		break;
		case NoRates:
			SetMatrix(matrix[0], len);
			
			for (i=0; i<numBases; i++) {
				*P=SetBase(matrix[0]+(*P<<2));
				P++;
			}
		break;
	}
}

void WriteSequence(int sampleNo, char *P)
{
int j;
	fprintf(stdout, "sample%-4d ", sampleNo);
	for (j=0; j<numBases; j++) {
		fputc(nucleotides[*P], stdout);
		P++;
	}
	fputc('\n', stdout);

}


