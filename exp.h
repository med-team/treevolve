/* header file for exp.c   */
/* (c) N Grassly 1997      */
/* Dept. Zoology, Oxford.  */

#ifndef _EXP_H_
#define _EXP_H_


#include "treevolve.h"

int EpiRoutine(Regime *pp);
double epifunc(double t);

#endif /* _EXP_H_ */

