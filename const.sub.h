/* header file for const.sub.c */
/* (c) N Grassly 1997          */
/* Dept. Zoology, Oxford.      */

#ifndef _CONSTSUB_H_
#define _CONSTSUB_H_


#include "treevolve.h"

int ConstSubRoutine(Regime *pp);

#endif /* _CONSTSUB_H_ */

