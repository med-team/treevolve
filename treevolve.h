/* Header treevolve.h     */
/* (c) Nick Grassly 1997  */
/* Dept. Zoology, Oxford. */

#ifndef _TREEVOLVE_H_
#define _TREEVOLVE_H_

#define MAX_NUMBER_REGIMES 10
#define MAX_NUMBER_SUBPOPS 100

#define MIN_NE 1.0e-15

typedef struct Node Node;
struct Node {
	Node *daughters[2];
	double time;
	short *ancestral;
	char *sequence;
	short type;/*0=ca 1=re 2=tip 3=looked at*/
	int cutBefore;
	int deme;
	Node *next;
	Node *previous;
};

typedef struct Regime Regime;
struct Regime {
	double t, N, e, r, m;
	int d;
};

extern int numBases, sampleSize;
extern double mutRate;
extern Node *first, *avail;
extern int ki[MAX_NUMBER_SUBPOPS], K, noRE, noCA, noMI;
extern double globTime, factr;
extern double genTimeInvVar;

/*prototypes*/
void Stack(Node *going);
long CalcGi(int deMe);
void Recombine(double t, int deme);
void Coalesce(double t, int deme);
void Migration(int deme, int numDemes);

#endif /* _TREEVOLVE_H_ */
