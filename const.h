/* header file for const.c */
/* (c) N Grassly 1997          */
/* Dept. Zoology, Oxford.      */

#ifndef _CONST_H_
#define _CONST_H_


#include "treevolve.h"

int ConstRoutine(Regime *pp);

#endif /* _CONST_H_ */

